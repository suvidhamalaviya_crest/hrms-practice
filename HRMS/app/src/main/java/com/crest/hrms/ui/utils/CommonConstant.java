package com.crest.hrms.ui.utils;

public interface CommonConstant {

    String USERNAME_PATTERN = "^[a-z]+\\.[a-z0-9]+$";
    String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&-+=()])(?=\\S+$).{8, 20}$";

    String SUCCESS_MESSAGE = "Logged In Successfully";
    String USER_NAME_REQUIRED = "User Name Required";
    String USERNAME_INVALID = "User Name Invalid";
    String PASSWORD_REQUIRED = "Password Required";
    String WRONG_CREDENTIAL_TRY_AGAIN = "Wrong Credential... Try Again!!!";
}
