package com.crest.hrms.ui.main.viewmodel;

import static com.crest.hrms.ui.utils.CommonConstant.USERNAME_PATTERN;

import android.text.TextUtils;
import android.util.Patterns;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

import com.crest.hrms.BR;
import com.crest.hrms.R;
import com.crest.hrms.data.model.User;
import com.crest.hrms.ui.utils.CommonConstant;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LoginViewModel extends BaseObservable {

    private User user;
    private static final Pattern pattern = Pattern.compile(USERNAME_PATTERN);


    @Bindable
    private String toastMessage = null;

    @Bindable
    private int imageResource = 0;

    public String getToastMessage() {
        return toastMessage;
    }

    public void setToastMessage(String toastMessage) {
        this.toastMessage = toastMessage;
        notifyPropertyChanged(BR.toastMessage);
        notifyPropertyChanged(BR.imageResource);
    }

    public int getImageResource() {
        return imageResource;
    }

    public void setImageResource(int imageResource) {
        this.imageResource = imageResource;
        notifyPropertyChanged(BR.imageResource);
    }

    @Bindable
    public String getUserEmail(){
        return user.getEmail();
    }

    public void setUserEmail(String email){
        user.setEmail(email);
        notifyPropertyChanged(BR.userEmail);
    }

    @Bindable
    public String getUserPassword(){
        return user.getPassword();
    }

    public void setUserPassword(String password){
        user.setPassword(password);
        notifyPropertyChanged(BR.userPassword);
    }

    public LoginViewModel(){
        user = new User("","");
    }

    public void onLoginButtonClicked(){
        if(getUserEmail().isEmpty()){
            setImageResource(R.drawable.ic_error);
            setToastMessage(CommonConstant.USER_NAME_REQUIRED);
        }
        else if(getUserPassword().isEmpty()){
            setImageResource(R.drawable.ic_error);
            setToastMessage(CommonConstant.PASSWORD_REQUIRED);
        }
        else if (isUserNameValid()){
            setImageResource(R.drawable.ic_error);
            setToastMessage(CommonConstant.USERNAME_INVALID);
        }
        else if(isInputDataValid()){
            setImageResource(R.drawable.ic_done);
            setToastMessage(CommonConstant.SUCCESS_MESSAGE);
        }
        else {
            setImageResource(R.drawable.ic_error);
            setToastMessage(CommonConstant.WRONG_CREDENTIAL_TRY_AGAIN);
        }
    }

    private boolean isUserNameValid() {
        Matcher matcher = pattern.matcher(getUserEmail());
        return matcher.matches();
    }

    public boolean isInputDataValid() {
        return getUserPassword().length() > 5;
    }
}

