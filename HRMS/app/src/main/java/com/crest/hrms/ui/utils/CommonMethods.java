package com.crest.hrms.ui.utils;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.crest.hrms.R;

public class CommonMethods {

    public static void showToast(Context context, String msg, int imageResource){
        TextView custom_toast_message;
        ImageView custom_toast_image;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );

        Drawable res = context.getResources().getDrawable(imageResource);

        View layout = inflater.inflate(R.layout.custom_toast,null);
        custom_toast_message = layout.findViewById(R.id.custom_toast_message);
        custom_toast_image = layout.findViewById(R.id.custom_toast_image);
        custom_toast_message.setText(msg);
        custom_toast_image.setImageDrawable(res);

        Toast toast = new Toast(context);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setView(layout);//setting the view of custom toast layout
        toast.show();

    }

}
