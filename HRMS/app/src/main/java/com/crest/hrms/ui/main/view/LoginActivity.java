package com.crest.hrms.ui.main.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.BindingAdapter;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;
import android.view.View;

import com.crest.hrms.R;
import com.crest.hrms.databinding.ActivityLoginBinding;
import com.crest.hrms.ui.main.viewmodel.LoginViewModel;
import com.crest.hrms.ui.utils.CommonMethods;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityLoginBinding activityLoginBinding = DataBindingUtil.setContentView(this,R.layout.activity_login);
        activityLoginBinding.setViewModel(new LoginViewModel());
        activityLoginBinding.executePendingBindings();
    }

    @BindingAdapter({"bind:toastMessage","bind:imageResource"})
    public static void runMe(View view, String message,int imageResource) {
        if (message != null)
            CommonMethods.showToast(view.getContext(),message,imageResource);
    }
}